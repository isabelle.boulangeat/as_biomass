
config.db.siddt <- function(){

require(RPostgreSQL)

#### Parametre ####
db_host     <<- "***"
db_name     <<- "basedtm"
db_user     <<- "***"
db_pw       <<- "***"

#### Connexion a la BDD ####

# Déclaration du driver a utiliser
drv <<- dbDriver("PostgreSQL")

#Préparation de la chaine de connexion
con <<- dbConnect(drv,
                  dbname = db_name ,
                  host = db_host,
                  port = "5432",
                  user = db_user,
                  password=db_pw)

#Declaration de l'encodage courant (souvent différent de la base qui est en UTF 8)
#postgresqlpqExec(con, "SET client_encoding = 'windows-1252'");
postgresqlpqExec(con, "SET client_encoding = 'UTF-8'")
return("connected")
}

#==============================================================

config.db.crocus <- function(){
  
  require(RPostgreSQL)
  
  #### Parametre ####
  db_host     <<- "***"
  db_name     <<- "db"
  db_user     <<- "***"
  db_pw       <<- "***"
  
  #### Connexion a la BDD ####
  
  # Déclaration du driver a utiliser
  drv <<- dbDriver("PostgreSQL")
  
  #Préparation de la chaine de connexion
  con <<- dbConnect(drv,
                    dbname = db_name ,
                    host = db_host,
                    port = "5432",
                    user = db_user,
                    password=db_pw)
  
  #Declaration de l'encodage courant (souvent différent de la base qui est en UTF 8)
  #postgresqlpqExec(con, "SET client_encoding = 'windows-1252'");
  postgresqlpqExec(con, "SET client_encoding = 'UTF-8'")
  return("connected")
}

#==============================================================

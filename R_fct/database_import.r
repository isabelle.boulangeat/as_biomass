

#==============================================================

import_biomass_from_db <- function(){

  require(dplyr)

  sql_biomass_h <- "SELECT * FROM sentinelle.biomasse_occurences"
  data_biomass_h <- dbGetQuery(con, sql_biomass_h)

  data_biomass_h$releve =
  unlist(lapply(data_biomass_h$ref_releve, function(x){
      paste(strsplit(x, "_")[[1]][-2], collapse="_")
  }))

  ### Attention! 3605 NA entries in hauteur
  na.entries = data_biomass_h[which(is.na(data_biomass_h$hauteur)),]
  unique(na.entries$ref_releve)
  write.table(na.entries, file = "na_entries.txt", sep="\t", row.names=F, quote =F)
  data_biomass_h = data_biomass_h[-which(is.na(data_biomass_h$hauteur)),]
  data_h = data_biomass_h %>% group_by(releve) %>% summarize(hmean=mean(hauteur), hsd=sd(hauteur), hmad = mad(hauteur))
  # str(data_h)
  # tail(data_h)

  sql_biomass_rel <- "SELECT * FROM sentinelle.biomasse_releves"
  data_biomass_rel <- dbGetQuery(con, sql_biomass_rel)

  # str(data_biomass_rel)
  data_biomass_rel$releve =
  unlist(lapply(data_biomass_rel$id_releve, function(x){
      paste(strsplit(x, "_")[[1]][-2], collapse="_")
  }))

  data_h_all = merge(data_h, data_biomass_rel[,c("releve", "date_releve", "ref_observateur")], by="releve")
  # str(data_h_all)

  data_h_all$ref_site = unlist(lapply(data_h_all$releve, function(x)strsplit(x, "_")[[1]][1]))

  dbGetQuery(con, "DROP TABLE chabli.biomasse_h")

  # sql_biomass_h_save <- "CREATE TABLE chabli.biomasse_h (
  #   ref_releve text,
  #   hmean real,
  #   hvar real,
  #   hmad real
  #   )"
  # dbGetQuery(con, sql_biomass_h_save)
  #
  dbWriteTable(con, name=c("chabli", "biomasse_h"), value=data_h_all, append=FALSE, row.names=FALSE)

  sql_biomass_site <- "SELECT * FROM sentinelle.biomasse_sites"
  data_biomass_site <- dbGetQuery(con, sql_biomass_site)

  # str(data_biomass_site)


  return(list(h = data_h_all, sites = data_biomass_site, otable = data_biomass_h))
}

#==============================================================

#==============================================================

import_meteo_from_db <- function(){
  
  # require(dplyr)
  # 
  # temp <- "SELECT * FROM crocus.loc_zonage_safran_france"
  # data_loc_nop <- dbGetQuery(con, sql_loc_nop)
  # 
  # # saveRDS(data_loc_nop, file = "data_loc_nop.rds")
  # 
  # 
  # 
}

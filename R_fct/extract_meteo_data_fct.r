
##############################################################################3
# generic fct
################################################################################

# calc variables gdd
#----------------------

identify_sveg <- function(snow_vec) {
  # #  (10j no snow = no snow)
  slide10_nosnow = which(mapply(function(X,Y) sum(snow_vec[X:Y]) ,1:(length(snow_vec)-9) , 10:length(snow_vec))<0.05)
  
  # longest continuous season without snow
  cont_seq = split(seq_along(slide10_nosnow), cumsum(c(TRUE, diff(slide10_nosnow) != 1)))
  unlist(cont_seq [which.max(lengths(cont_seq))])
  slide10_nosnow = slide10_nosnow[unlist(cont_seq [which.max(lengths(cont_seq))])]
  
  return(slide10_nosnow)
}

calc_vars_gdd <- function(tab_vars, tbase=0, snow.col = "sd", temp.col = "soil_temp5mm",  date.col = "ddate", farenheit=TRUE){
  require(lubridate)
  require(dplyr)
  tab_vars = as.data.frame(tab_vars)
  colnames(tab_vars)[which(colnames(tab_vars)==snow.col)] = "snow_depth"
  colnames(tab_vars)[which(colnames(tab_vars)==temp.col)] = "temp"
  colnames(tab_vars)[which(colnames(tab_vars)==date.col)] = "ddate"
  tab_vars$ddate = as.Date(tab_vars$ddate)

  sveg = identify_sveg(tab_vars$snow_depth)
  lsd = sveg[1]
  fsd = sveg[length(sveg)]
  
  output = calc_vars_gdd_from_lsd (tab_vars, lsd, end_obs=fsd, tbase=0, temp.col =  temp.col, date.col = date.col, farenheit = farenheit)
  
  if((lsd-30)> 0) output$previous30_runoff = sum(tab_vars$runoff[(lsd-30):lsd]) else output$previous30_runoff = NA
  
  return(output)
 }

#test 
# require(lubridate)
#path_csv_snow = "_data_raw/"
# dat1 =read.csv(paste0(path_csv_snow, "crocus_snow_alp_safran_reanalysis_nn_",as.character(y),"_selectloc.csv"))
# dat2 =read.csv(paste0(path_csv_snow, "crocus_snow_alp_safran_reanalysis_nn_",as.character(y-1),"_selectloc.csv"))
# data_sveg = bind_rows(dat2, dat1)
# data_sveg$year = year(data_sveg$ddate)
# data_sveg %>% filter(year == 2016 & loc == 1707) %>% calc_vars_gdd()


calc_vars_gdd_from_lsd <- function(tab_vars, lsd, end_obs, tbase=0, temp.col = "Tgdd", date.col = "ddate", farenheit = TRUE){
  
  if(farenheit) {
    temp_zero = 273.15
  }else temp_zero = 0
  
  require(lubridate)
  require(dplyr)
  tab_vars = as.data.frame(tab_vars)
  colnames(tab_vars)[which(colnames(tab_vars)==temp.col)] = "temp"
  colnames(tab_vars)[which(colnames(tab_vars)==date.col)] = "ddate"
  tab_vars$ddate = as.Date(tab_vars$ddate)
  
  gdd_tab = tab_vars[lsd:nrow(tab_vars),] %>% mutate(gdd = temp - temp_zero - tbase)  %>%
    mutate(gdd = ifelse(gdd<0, 0, gdd)) %>%
    mutate(cumgdd = cumsum(gdd))
  if(max(91, lsd)==91) {
    gdd_tab2 = tab_vars[91:nrow(tab_vars),] %>% mutate(gdd = temp - temp_zero - tbase)  %>%
      mutate(gdd = ifelse(gdd<0, 0, gdd)) %>%
      mutate(cumgdd = cumsum(gdd))
  }else gdd_tab2 = gdd_tab
  gdd300 = lubridate::yday(first(gdd_tab[which(gdd_tab$cumgdd>300),"ddate"]))
  gdd600 = lubridate::yday(first(gdd_tab[which(gdd_tab$cumgdd>600),"ddate"]))
  gdd900 = lubridate::yday(first(gdd_tab[which(gdd_tab$cumgdd>900),"ddate"]))
  index300 = first(which(gdd_tab$cumgdd>300))
  if(is.na(index300)) {
    ndf300 = NA
    mst300 = NA
    maxt300 = NA
  }else{
    ndf300 = length(which((gdd_tab[1:index300 ,"temp"]-temp_zero)<0))
    mst300 = min(gdd_tab[1:index300 ,"temp"]-temp_zero)
    maxt300 = max(gdd_tab[1:index300, "temp"]-temp_zero)
  }
  
  ## spei

  return(data.frame(cumgdd = gdd_tab$cumgdd[end_obs-lsd], # end_obs index = end_obs - lsd dans gdd_tab
                    cumgdd60 = gdd_tab2$cumgdd[60],
                    cumgdd30 = gdd_tab2$cumgdd[30],
                    daygdd300 = gdd300,
                    daygdd600 = gdd600,
                    daygdd900 = gdd900,
                    speedgdd300 = gdd300-lsd,
                    speedgdd300600 = gdd600-gdd300,
                    ndayfrost = length(which((gdd_tab$temp[1:(end_obs-lsd)]-temp_zero)<0)),
                    ndayfrost300 = ndf300,
                    minTemp = min(gdd_tab$temp[1:(end_obs-lsd)]-temp_zero), 
                    maxTemp = max(gdd_tab$temp[1:(end_obs-lsd)]-temp_zero),
                    minTemp300 = mst300, 
                    maxTemp300 = maxt300,
                    LSD = lsd, 
                    end_season = end_obs
  ))
}



##############################################################################3
# extract simulations 
################################################################################

extract_simuls_flat = function(point_attributes, simul_attributes, pcol.massif = "massif", pcol.elev = "alti", scol.massif = "id_mf", scol.elev = "alt", scol.id = "nop"){
  
  colnames(point_attributes)[which(colnames(point_attributes)==pcol.massif)] = "massif"
  colnames(point_attributes)[which(colnames(point_attributes)==pcol.elev)] = "alti"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.massif)] = "id_mf"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.elev)] = "alt"
  
  sim_select = simul_attributes %>% filter(id_mf == point_attributes$massif) %>% 
    filter(alt < point_attributes$alti+300) %>% filter(alt > point_attributes$alti-300)
  w.alti = (300-abs(sim_select$alt - point_attributes$alti))/150/nrow(sim_select)
  
  return(data.frame(simuls_gid = sim_select[,scol.id], weights = w.alti/sum(w.alti)))
  
}

extract_simuls = function(point_attributes, simul_attributes, pcol.massif = "massif", pcol.elev = "alti", pcol.slope = "slope" , pcol.aspect= "aspect",  scol.massif = "id_mf", scol.elev = "alt", scol.slope = "slope", scol.aspect = "aspect", scol.id = "gid"){
  
  colnames(point_attributes)[which(colnames(point_attributes)==pcol.massif)] = "massif"
  colnames(point_attributes)[which(colnames(point_attributes)==pcol.elev)] = "alti"
  colnames(point_attributes)[which(colnames(point_attributes)==pcol.slope)] = "slope"
  colnames(point_attributes)[which(colnames(point_attributes)==pcol.aspect)] = "aspect"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.massif)] = "id_mf"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.elev)] = "alt"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.slope)] = "slope"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.aspect)] = "aspect"
  colnames(simul_attributes)[which(colnames(simul_attributes)==scol.id)] = "gid"
  
  sim_select = simul_attributes %>% filter(id_mf == point_attributes$massif) %>% 
    filter(alt < point_attributes$alti+300) %>% filter(alt > point_attributes$alti-300)
  
  if(point_attributes$slope>10){
    sim_select = sim_select %>% filter(slope < point_attributes$slope+10) %>% filter(slope > point_attributes$slope-10)
    if(point_attributes$aspect > 45 && point_attributes$aspect < 315) {
      sim_select = sim_select %>% filter(aspect < point_attributes$aspect+45) %>% filter(aspect > point_attributes$aspect-45)
    }else if (point_attributes$aspect<45){
      sim_select = sim_select %>% filter(aspect %in% c(0, 45))
    }else if(point_attributes$aspect>315){
      sim_select = sim_select %>% filter(aspect %in% c(0, 315))  
    }
    
    ### plat
  }else{
    sim_select = sim_select %>% filter(aspect == -1)
  }
  
  #calc weights
  w.alti = (300-abs(sim_select$alt - point_attributes$alti))/150/nrow(sim_select)
  w.slope = (10-abs(sim_select$slope - point_attributes$slope))/5/nrow(sim_select)
  w.aspect1 = abs(sim_select$aspect - ((point_attributes$aspect+45) %% 360))
  w.aspect2 = abs(sim_select$aspect - ((point_attributes$aspect-45) %% 360)) 
  w.aspect = apply(cbind(w.aspect1, w.aspect2), 1, min)
  
  w.aspect = (45- w.aspect) / (45/2)   /nrow(sim_select)
  w.aspect[which(sim_select$aspect == -1)] =  1/nrow(sim_select)
  
  
  w.tot = apply(cbind(w.alti, w.slope, w.aspect), 1, sum)
  w.tot = w.tot/sum(w.tot)
  
  return(data.frame(simuls_gid = sim_select$gid, weights = w.tot))
  
}

# tests
# loc_nop <- read.csv("_data_raw/crocus_loc_zonage_safran_france.csv")

# loc_nop[loc_nop$gid %in% extract_simuls(data.frame(alti = 2150, aspect = 12, slope = 5, massif = 22 ), loc_nop)$simuls_gid,]
# 
# loc_nop[loc_nop$gid %in% extract_simuls(data.frame(alti = 2150, aspect = 12, slope = 25, massif = 22 ), loc_nop)$simuls_gid,]
# 
# loc_nop[loc_nop$gid %in% extract_simuls(data.frame(alti = 2150, aspect = 345, slope = 25, massif = 22 ), loc_nop)$simuls_gid,]

##############################################################################3
# climatologies
################################################################################

calc_climato <- function(path, years, meteo_function, gid.colname = "loc", ...){
  
  output_list <- lapply(years, function(y){
    meteo_function(path, y, ...)
  } )
  
  names(output_list) = years

  
  output = output_list %>% bind_rows(.id = "year")
  colnames(output)[which(colnames(output)==gid.colname)] = "loc"

  output_average = output %>% group_by(loc) %>% summarise_at(vars(-group_cols(),-year), mean)
  
  return(output_average)
}

#test
# calc_climato(path = "_data_raw/", years = c(2016, 2017), meteo_function = croscut_calc_month_vars)

# calc_climato(path = "_data_raw/", years = c(2016, 2017), meteo_function = crosscut_calc_gdd_periods)

##############################################################################3
# extract climatic variables from CROSSCUT 
################################################################################

croscut_calc_month_vars <- function(path_csv_snow, y, months = c(4:8) ){
  print(y)
  require(lubridate)
  dat1 =read.csv(paste0(path_csv_snow, "crocus_snow_alp_safran_reanalysis_nn_",as.character(y),"_selectloc.csv"))
  dat2 =read.csv(paste0(path_csv_snow, "crocus_snow_alp_safran_reanalysis_nn_",as.character(y-1),"_selectloc.csv"))
  data_sveg = bind_rows(dat2, dat1)
  data_sveg$ddate = as.Date(data_sveg$ddate)
  data_sveg$month = lubridate::month(data_sveg$ddate)
  data_sveg$year = lubridate::year(data_sveg$ddate)
  
  output = data_sveg %>% filter(year == y) %>% group_by(loc, month) %>% summarise(soil_temp5mm = mean(soil_temp5mm), soil_temp8cm = mean(soil_temp8cm), runoff = mean(runoff), snowdepth = mean(sd))
  
  output = output %>% filter(month %in% months) %>% 
    pivot_wider(
    names_from = month,
    names_glue = "{.value}_month{month}",
    values_from = c(soil_temp5mm, soil_temp8cm, runoff, snowdepth)
  )
  
  return(output)
}
# test
# croscut_calc_month_vars("_data_raw/", 2016)

crosscut_calc_gdd_periods <- function(path_csv_snow= "_data_raw/", y, tbase = 0){
  print(y)
  require(lubridate)
  require(dplyr)
  dat1 =read.csv(paste0(path_csv_snow, "crocus_snow_alp_safran_reanalysis_nn_",as.character(y),"_selectloc.csv"))
  dat2 =read.csv(paste0(path_csv_snow, "crocus_snow_alp_safran_reanalysis_nn_",as.character(y-1),"_selectloc.csv"))
  data_sveg = bind_rows(dat2, dat1)
  data_sveg$year = year(data_sveg$ddate)
  data_sveg = data_sveg %>% filter(year == y)
  
  output_loc = data_sveg %>% group_by(loc) %>% dplyr::select(loc) %>% pull() %>% unique()
  
  output = data_sveg %>% group_by(loc) %>% group_map( ~ calc_vars_gdd(.x, farenheit = TRUE)) %>% bind_rows()
  
  output$loc = output_loc
  
  
  return(output)
  
}

#test
# crosscut_calc_gdd_periods("_data_raw/", 2016)


##############################################################################3
# MANIP and extract from NCDAT (allslope)
################################################################################

extract_ncdat_meteo <- function(path, variables, NoPs){
 
  nc.read = nc_open(path)
  
  # nc.read
  # attributes(nc.read)$name
  # attributes(nc.read$var)$name
  # attributes(nc.read$dim)$name
  # ncvar_get(nc.read, "Tair")->aa
  
  time<-ncvar_get(nc.read,"time")
  tunits<-ncatt_get(nc.read,"time",attname="units")
  tustr<-strsplit(tunits$value, " ")
  
  print("extracting from ...")
  print(tunits$value)
  # print(length(time))
  time2 = c(rep(0, 18), rep(1:364, each = 24), rep(365, 7))
  
  if(length(time)==length(time2)) dates<-as.Date(time2,origin=unlist(tustr)[3]) 
  else {
    # warning("")
   time[1:length(time2)] = time2 
   time[length(time2):length(time)] = 365
   dates<-as.Date(time,origin=unlist(tustr)[3]) 
  }
  
  
  if(length(unique(NoPs))>1) {
    nc.data = abind(lapply(variables, function(x) ncvar_get(nc.read,x)[unique(NoPs),]), along = 3)
    
    dimnames(nc.data)[[3]] = variables
    
    require(purrr)
    nc.data = array_branch(nc.data, 1)
    names(nc.data) = NoPs
    
  } else {
    nc.data = abind(lapply(variables, function(x) ncvar_get(nc.read,x)[unique(NoPs),]), along = 2)
    nc.data = as.data.frame(nc.data)
    colnames(nc.data) = variables
    
    nc.data = list(nc.data)
    names(nc.data) = NoPs
  }
  nc_close(nc.read)
  return(list(data = nc.data, dates=dates))
  
  nc_close(nc.read)
  return(list(data = nc.data, dates = dates))
}

##############################################################################3
# SAISON INPUT VARS FROM NCDAT FILES
################################################################################

extract_season_vars_meteo <- function(path_data_allslopes, year, NoPs, 
    variables_meteo = c("Tair", "Rainf") )# HUMREL, Wind, NEB)
    {
  
  # year = 2016
  # NoPs = 1074
  # NoPs = c(1074, 245)
  # path_data_allslopes="/Users/isabelleboulangeat/Documents/_data_temp/alp_allslopes/"
  
  # library required
  require(tidync)
  require(ncmeta)
  require(tidyr)
  require(ncdf4)
  require(ncdf4.helpers)

  # check filepath dataset
  if(!(length(list.files(path_data_allslopes)) > 0) ) stop("not able to connect to data path or empty path")
  
  # extract ncdf
  
  dat1_meteo = extract_ncdat_meteo(paste0(path_data_allslopes, "FORCING_",as.character(year),"080106_",as.character(year+1),"080106.nc"), variables_meteo, NoPs)
  dat2_meteo = extract_ncdat_meteo(paste0(path_data_allslopes, "FORCING_",as.character(year-1),"080106_",as.character(year),"080106.nc"), variables_meteo, NoPs)
  
  datAll_meteo = lapply(1:length(dat1_meteo$data), function(x) rbind(dat2_meteo$data[[x]], dat1_meteo$data[[x]]))
  names(datAll_meteo) = names(dat1_meteo$data)
  datAll_meteo_dates = c(dat2_meteo$dates, dat1_meteo$dates)
  
return(list(dat_meteo = datAll_meteo, dates_meteo = datAll_meteo_dates, NoPs = NoPs ))  
  
}

##############################################################################3
### CALC Tmin pour frost, Tgdd = (Tmax+Tmin)/2 FROM AIR TEMP
##############################################################################3

calc_temperatures <- function(dat_meteo, dates){
# dat_meteo = datAll_meteo
# dates = datAll_meteo_dates
  
  require(dplyr)
  
  output = list()
  for (i in 1:length(dat_meteo)){
  output [[i]] <- data.frame(dat_meteo[[i]]) %>% 
    mutate(days = as.factor(dates)) %>%
    group_by(days) %>%
    summarise(Tmin = min(Tair) - 273.15, Tmax = max(Tair)- 273.15, Tgdd = (max(Tair)+min(Tair))/2 -273.15  , n = n())
  }
  names(output) <- names(dat_meteo)
  return(output)
}

calc_precip <- function(dat_meteo, dates){
  # dat_meteo = datAll_meteo
  # dates = datAll_meteo_dates
  
  require(dplyr)
  
  output = list()
  for (i in 1:length(dat_meteo)){
    output [[i]] <- data.frame(dat_meteo[[i]]) %>% 
      mutate(days = as.factor(dates)) %>%
      group_by(days) %>%
      summarise(PP = sum(Rainf), n = n())
  }
  names(output) <- names(dat_meteo)
  return(output)
}


##############################################################################3
# SAISON VARIABLES
################################################################################

safran_calc_month_vars <- function(path_data_allslopes, y, NoPs, months = 4:8 ){
  print(y)
  require(dplyr)
  data = extract_season_vars_meteo(path_data_allslopes, y, as.integer(NoPs$simuls_nop))
  print("data extracted from ncdf")
  data_temp = calc_temperatures (data$dat_meteo, data$dates_meteo)
  data_precip = calc_precip (data$dat_meteo, data$dates_meteo) %>% bind_rows(.id = "nop")  %>% dplyr::select(nop, days, PP)
  
  data_sveg = data_temp %>% bind_rows(.id = "nop") %>% full_join(data_precip, by = c("nop", "days"))
  data_sveg$ddate = as.Date(data_sveg$days)
  data_sveg$month = lubridate::month(data_sveg$ddate)
  data_sveg$year = lubridate::year(data_sveg$ddate)
 
  require(SPEI) 
  output = data_sveg %>% filter(year == y) %>% left_join(NoPs, by = c("nop" = "simuls_nop" )) %>% group_by(nop, month) %>% summarise(Tmin = mean(Tmin), Tmax=mean(Tmax), Tgdd=mean(Tgdd), precip = sum(PP) , alti = mean(alti), lat =mean(latitude)) %>% ungroup()
  
  output$PET = hargreaves(output$Tmin, output$Tmax, lat = mean(output$lat), Pre = output$precip, verbose = FALSE)
  
  output$WBAL = output$precip - output$PET
  
  output = output %>% filter(month %in% months) %>% 
    pivot_wider(
      names_from = month,
      names_glue = "{.value}_month{month}",
      values_from = c(Tmin, Tmax, Tgdd, precip, PET, WBAL)
    )

  return(output)

}

#test
# safran_calc_month_vars(path_data_allslopes, 2017, c(128,129))

##############################################################################3
#   LSD ou date base -> date releve
################################################################################


safran_calc_date_vars <- function(path_data_allslopes, y, tab_date_nop, nop.colname = "nop", date.colname = "date_releve", tbase = 0, dbase = 91, lsd.colname = ""){
 
  print(y)
  tab_date_nop = tab_date_nop %>% filter(year == y)
  colnames(tab_date_nop)[which(colnames(tab_date_nop)==date.colname)] = "date_releve"
  colnames(tab_date_nop)[which(colnames(tab_date_nop)==nop.colname)] = "nop"
  if( dbase == "lsd" ) colnames(tab_date_nop)[which(colnames(tab_date_nop)==lsd.colname)] = "lsd"
    
  NoPs = unique(tab_date_nop$nop)
  require(lubridate)
  data = extract_season_vars_meteo(path_data_allslopes, y, NoPs)
  print("data extracted from ncdf")
  data_temp = calc_temperatures (data$dat_meteo, data$dates_meteo)
  data_precip = calc_precip (data$dat_meteo, data$dates_meteo) %>% bind_rows(.id = "nop")  %>% dplyr::select(nop, days, PP)
  
  data_sveg = data_temp %>% bind_rows(.id = "nop") %>% full_join(data_precip, by = c("nop", "days"))
  data_sveg$ddate = as.Date(data_sveg$days)
  data_sveg$day = lubridate::yday(data_sveg$ddate)
  data_sveg$year = lubridate::year(data_sveg$ddate)
  data_sveg = data_sveg %>% filter( year == y)
  
  for(i in 1:nrow(tab_date_nop)){
    no = tab_date_nop[i, "nop"]
    drel = tab_date_nop[i, "date_releve"]
    
    if(dbase == "lsd"){
      lsd1 = tab_date_nop[i, "lsd"]
      dperiod = data_sveg %>% filter(nop == no) %>% slice(lsd1:yday(drel))
    }else{
      dperiod = data_sveg %>% filter(nop == no) %>% slice(dbase:yday(drel))
    }
    
    temp =    dperiod[, "Tgdd"] %>% 
      transmute(Tgdd = Tgdd - tbase)  %>% 
      transmute(Tgdd = ifelse(Tgdd<0, 0, Tgdd))
    
    tab_date_nop$cumgdd_period[i] = sum(temp)
    tab_date_nop$ndayfrost_period[i] = sum(dperiod$Tmin<0)
    tab_date_nop$tmin_min_period[i] = min(dperiod$Tmin)
    tab_date_nop$tmax_max_period[i] = max(dperiod$Tmax)
    tab_date_nop$tgdd_period[i] = mean(dperiod$Tgdd)
    tab_date_nop$precip_period[i] = sum(dperiod$PP)
    
    # tab_date_nop$PET = hargreaves(tab_date_nop$Tmin, tab_date_nop$Tmax, lat = mean(output$lat), Pre = output$precip, verbose = FALSE)
    # 
    # output$WBAL = output$precip - output$PET

    }

  return(tab_date_nop)

}

#test
# safran_calc_date_vars (path_data_allslopes, y= 2016,tab, nop.colname = "simuls_nop")
# safran_calc_date_vars (path_data_allslopes, y= 2016,tab, nop.colname = "simuls_nop", dbase = "lsd", lsd.colname = "LSD")

##############################################################################3
# GDD PERIODS - avec LSD as parameter
################################################################################

safran_calc_gdd_periods <- function(path_data_allslopes, y, tab_lsd_nop_date, nop.colname = "nop", lsd.colname = "LSD",end.colname = "end_season",  tbase = 0, farenheit){
  
  tab_lsd_nop_date = tab_lsd_nop_date %>% filter(year == y)
  colnames(tab_lsd_nop_date)[which(colnames(tab_lsd_nop_date)==nop.colname)] = "nop"
  colnames(tab_lsd_nop_date)[which(colnames(tab_lsd_nop_date)==lsd.colname)] = "lsd"
  colnames(tab_lsd_nop_date)[which(colnames(tab_lsd_nop_date)==end.colname)] = "end_season"
  
  print(y)
  require(dplyr)
  data = extract_season_vars_meteo(path_data_allslopes, y, unique(tab_lsd_nop_date$nop))
  print("data extracted from ncdf")
  data_temp = calc_temperatures (data$dat_meteo, data$dates_meteo)
  
  data_precip = calc_precip (data$dat_meteo, data$dates_meteo) %>% bind_rows(.id = "nop")  %>% dplyr::select(nop, days, PP)
  
  data_sveg = data_temp %>% bind_rows(.id = "nop") %>% full_join(data_precip, by = c("nop", "days"))
  data_sveg$ddate = as.Date(data_sveg$days)
  data_sveg$month = lubridate::month(data_sveg$ddate)
  data_sveg$year = lubridate::year(data_sveg$ddate)
  data_sveg = data_sveg %>% filter( year == y)
  
  result= lapply(1:nrow(tab_lsd_nop_date), function(i){
    no = tab_lsd_nop_date[i, "nop"]
    lsd = as.integer(tab_lsd_nop_date[i, "lsd"])
    drel = tab_lsd_nop_date[i, "end_season"]
    dnop = data_sveg %>% filter (nop == no)
    
    tabres = calc_vars_gdd_from_lsd(dnop, lsd, end_obs = drel, tbase=0, temp.col = "Tgdd", date.col = "ddate", farenheit = farenheit)
    
    tabres$cumprecip = sum(dnop[lsd:drel,"PP"])
    if(!is.na(tabres$daygdd300))
    {
      tabres$precip300 = sum(dnop[lsd:tabres$daygdd300,"PP"])
      tabres$ndayfrost300_Tmin_min = sum(dnop[lsd:tabres$daygdd300,"Tmin"]<0)
      tabres$minTemp300_Tmin_min = min(dnop[lsd:tabres$daygdd300,"Tmin"])
      tabres$maxTemp300_Tmax_max = max(dnop[lsd:tabres$daygdd300,"Tmax"])
      
    }else{
      tabres$precip300 = tabres$ndayfrost300_Tmin_min = tabres$minTemp300_Tmin_min = tabres$maxTemp300_Tmax_max = NA
    }

    
    return(tabres)
  }) %>% bind_rows()
  
  output = bind_cols(tab_lsd_nop_date, result)
  
  return(output)
  
  

}

# safran_calc_gdd_periods(path_data_allslopes, 2017, tab, nop.colname = "simuls_nop", farenheit = FALSE)


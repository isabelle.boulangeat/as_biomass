---
title: "README"
author: "Isabelle Boulangeat"
date: today
output:
  html_document:
    keep_md: yes
    variant: markdown_github
  pdf_document: 
    keep_md: yes
editor_options:
  chunk_output_type: console
always_allow_html: yes
---


# Biomasse data

## Code d'extraction from siddt database
voir workflow0



## Description générale
- Hauteur moyenne mesurée par milieu
- Variation inter-relevés (spatiale ou temporelle)
- Variation intra-relevés (hétérogénéité estimée sur les 80 points de mesure de hauteur)



### Par milieu (variations spatiales et temporelles)

![](README_files/figure-html/unnamed-chunk-1-1.png)<!-- -->

![](README_files/figure-html/unnamed-chunk-2-1.png)<!-- -->


### Séries temporelles par placette (variations temporelles seules)
![](README_files/figure-html/unnamed-chunk-3-1.png)<!-- -->


### Calibration et qualité de mesure


![](README_files/figure-html/calibmean-1.png)<!-- -->

![](README_files/figure-html/caliberror-1.png)<!-- -->


Erreur de mesure et variation spatiale:
![](README_files/figure-html/varspat-1.png)<!-- -->

Erreure de mesure et variation temporelle:
![](README_files/figure-html/vartemp-1.png)<!-- -->

On peut considérer que les sites qui varient en dessous de Mean Absolute Deviation = 1cm ne varient pas assez pour passer au dessus de notre seuil de détection.

Il s'agit de :


```
## # A tibble: 55 × 3
##    ref_site  hmad ref_typoveg
##    <chr>    <dbl> <chr>      
##  1 BELRIV02 0.917 QUE        
##  2 BELRIV04 0.213 ALP        
##  3 ECRCRO01 0.630 SUB        
##  4 ECRCRO03 0.241 PROD       
##  5 ECRCRO04 0.927 ALP        
##  6 ECRCRO05 0.445 NIV        
##  7 ECRGRA01 0.927 QUE        
##  8 ECRGRA03 0.667 QUE        
##  9 ECRGRA04 0.630 ALP        
## 10 ECRGRA05 0.899 ALP        
## # ℹ 45 more rows
```

```
## 
##  ALP BOMB BRAC ECOR ENHE  HUM  NAR  NIV PROD  QUE  SUB 
##   10    5    1    6    4    1    7    6    3    4    8
```

### Variabilité intra placette pour une date de mesure
Mesure l'hétérogénéité de végétation au sein de la placette

sd vs mad
![](README_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

![](README_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

![](README_files/figure-html/unnamed-chunk-7-1.png)<!-- -->



# Site characteristics

```
## 
##  ALP BOMB BRAC EBOU ECOR ENHE  HUM LAND  NAR  NIV PROD  QUE  SUB 
##   15    7    3    2   10    7    1    2   11    8   20   12   31
```

![](README_files/figure-html/sites_typeveg-1.png)<!-- -->

```
## 
##  ALP BOMB BRAC EBOU ECOR ENHE  HUM  NAR  NIV PROD  QUE  SUB 
##   15    7    2    1   10    7    1   11    8   20   12   28
```

![](README_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

On retrouve les étagements des types de vegetation. Certains sont assez étalés en altitude.

![](README_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

La pente est peu discriminante des types de vegetation sauf pour les eboulis et les pelouses enherbées.



# Releves (sites x date) and climate


```
## 
##  ALP BOMB BRAC EBOU ECOR ENHE  HUM  NAR  NIV PROD  QUE  SUB 
##   62   24    9    3   69   43    3   46   31  113   47  141
```

Etendu du jeu de données par type de végétation


```
## 
##  ALP BOMB BRAC EBOU ECOR ENHE  HUM  NAR  NIV PROD  QUE  SUB 
##   62   24    9    3   69   43    3   46   31  113   47  141
```

![](README_files/figure-html/plotveg-1.png)<!-- -->

Certains milieux ne sont pas assez représentés et on ne peut pas les analyser séparemment. A grouper (BRAC)? A ne pas poursuivre (EBOUL, HUM) pour la biomasse?

## date de denneigement
![](README_files/figure-html/unnamed-chunk-11-1.png)<!-- -->

La date de denneigement separe bien les types de vegetation.
- 75 = 16 mars
- 100 = 10 avril
- 125 = 5 mai
- 150 = 31 mai
- 175 = 24 juin

## comparison Tair and Tsoil
Tair :
- Tmin calculée à partir des températures au pas horaire qui sont intégrées au pas journalier comme (max+min)/2
Tsoil :
- Tsoil de surface (5mm) donné par safran-crocus au pas journalier

Les minimums et les cumuls gdd saisonniers sont calculés pendant la saison de végétation (jusqu'au retour de la neige)

![](README_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

Pour la plupart des milieux le sol a un effet "buffer". Cela est moins vrai pour les milieux SUB/PROD/NAR


Regard sur les minimales:
![](README_files/figure-html/unnamed-chunk-13-1.png)<!-- -->

Effet "buffer" pour tous les milieux sur la minimale.

![](README_files/figure-html/unnamed-chunk-14-1.png)<!-- -->

Bien que les températures air et sol soient différentes, le cumul des degré jours est très corrélé, avec un cumul systématiquement plus élevé pour les températures de l'air.


## Gdd et date de mesure

![](README_files/figure-html/unnamed-chunk-15-1.png)<!-- -->

La plupart des observations, quelquesoient les types de végétation, sont faites vers 500-600 degré-jours. Certaines observations sont faites beaucoup plus tard.


```
##                  releve date_releve ref_typoveg alti
## 1   BELRIV01_07_08_2017  2017-08-07         QUE 1927
## 3   BELRIV01_18_09_2019  2019-09-18         QUE 1927
## 5   BELRIV01_21_09_2017  2017-09-21         QUE 1927
## 7   BELRIV02_07_08_2017  2017-08-07         QUE 2076
## 9   BELRIV02_18_09_2019  2019-09-18         QUE 2076
## 11  BELRIV02_21_09_2017  2017-09-21         QUE 2076
## 13  BELRIV03_07_08_2017  2017-08-07         SUB 2188
## 15  BELRIV03_18_09_2019  2019-09-18         SUB 2188
## 17  BELRIV03_21_09_2017  2017-09-21         SUB 2188
## 20  BELRIV04_18_09_2019  2019-09-18         ALP 2345
## 21  BELRIV04_21_09_2017  2017-09-21         ALP 2345
## 32  ECRCHA02_04_07_2017  2017-07-04        PROD 1663
## 33  ECRCHA02_07_07_2015  2015-07-07        PROD 1663
## 34  ECRCHA02_10_07_2018  2018-07-10        PROD 1663
## 35  ECRCHA02_13_07_2012  2012-07-13        PROD 1663
## 58  ECRCHA05_17_07_2015  2015-07-17         QUE 1988
## 66  ECRCHA06_17_07_2015  2015-07-17         QUE 1984
## 74  ECRCHA07_28_07_2017  2017-07-28        ECOR 2012
## 83  ECRCHA08_27_07_2015  2015-07-27        ECOR 2056
## 84  ECRCHA08_30_07_2017  2017-07-30        ECOR 2056
## 86  ECRCHA09_24_07_2017  2017-07-24        PROD 2035
## 91  ECRCHA09_27_07_2015  2015-07-27        PROD 2035
## 97  ECRCHA10_25_07_2015  2015-07-25         NAR 2091
## 108 ECRCHA11_30_07_2015  2015-07-30         SUB 2126
## 114 ECRCHA12_27_07_2017  2017-07-27         SUB 2045
## 115 ECRCHA12_29_07_2015  2015-07-29         SUB 2045
## 135 ECRDIS02_17_07_2018  2018-07-17         QUE 2017
## 162 ECRLAN01_05_07_2018  2018-07-05         SUB 1731
## 202 ECRSUR03_10_07_2018  2018-07-10         SUB 1873
## 203 ECRSUR03_12_07_2017  2017-07-12         SUB 1873
## 204 ECRSUR03_18_07_2019  2019-07-18         SUB 1873
## 211 MERALP03_17_09_2019  2019-09-17         QUE 2144
## 212 MERALP04_06_08_2019  2019-08-06        PROD 2320
## 215 MERCHA01_01_08_2018  2018-08-01         SUB 2048
## 216 MERCHA01_22_07_2019  2019-07-22         SUB 2048
## 217 MERCHA02_01_08_2018  2018-08-01         NAR 1960
## 218 MERCHA02_22_07_2019  2019-07-22         NAR 1960
## 219 MERCHA03_22_07_2019  2019-07-22        PROD 2033
## 220 MERDEM02_26_07_2019  2019-07-26        BRAC 1924
## 225 MERLAU01_23_08_2019  2019-08-23         ALP 2338
## 226 MERLAU02_23_08_2019  2019-08-23         NIV 2442
## 228 MERPIS02_12_08_2019  2019-08-12        PROD 2339
## 235 UBABAS01_01_08_2019  2019-08-01         NAR 2105
## 237 UBABAS01_26_07_2018  2018-07-26         NAR 2105
## 238 UBABAS02_01_08_2019  2019-08-01         NAR 2095
## 240 UBABAS02_26_07_2018  2018-07-26         NAR 2095
## 241 UBABAS03_01_08_2019  2019-08-01         SUB 2074
## 243 UBABAS03_26_07_2018  2018-07-26         SUB 2074
## 244 UBABAS04_01_08_2019  2019-08-01         HUM 2011
## 246 UBABAS04_26_07_2018  2018-07-26         HUM 2011
## 418 VANVAL03_16_08_2017  2017-08-16        ENHE 2316
## 419 VANVAL03_20_08_2019  2019-08-20        ENHE 2316
## 420 VANVAL03_2012_08_27  2012-08-27        ENHE 2316
## 421 VANVAL03_2013_09_05  2013-09-05        ENHE 2316
## 423 VANVAL03_2015_09_01  2015-09-01        ENHE 2316
## 424 VANVAL03_2016_08_26  2016-08-26        ENHE 2316
## 425 VANVAL04_16_08_2017  2017-08-16         NIV 2282
## 426 VANVAL04_20_08_2019  2019-08-20         NIV 2282
## 427 VANVAL04_2012_08_27  2012-08-27         NIV 2282
## 428 VANVAL04_2013_09_05  2013-09-05         NIV 2282
## 429 VANVAL04_2014_09_02  2014-09-02         NIV 2282
## 430 VANVAL04_2015_09_01  2015-09-01         NIV 2282
## 431 VANVAL04_2016_08_26  2016-08-26         NIV 2282
## 517 VERDAR01_02_07_2015  2015-07-02         SUB 1303
## 518 VERDAR01_02_07_2018  2018-07-02         SUB 1303
## 521 VERDAR01_07_07_2017  2017-07-07         SUB 1303
## 523 VERDAR01_27_07_2012  2012-07-27         SUB 1303
## 525 VERDAR02_02_07_2015  2015-07-02         SUB 1333
## 526 VERDAR02_02_07_2018  2018-07-02         SUB 1333
## 529 VERDAR02_07_07_2017  2017-07-07         SUB 1333
## 531 VERDAR03_02_08_2017  2017-08-02         SUB 1939
## 532 VERDAR03_02_08_2018  2018-08-02         SUB 1939
## 535 VERDAR03_13_08_2019  2019-08-13         SUB 1939
## 537 VERDAR03_30_07_2015  2015-07-30         SUB 1939
## 545 VERJOC01_30_07_2012  2012-07-30        PROD 1473
## 553 VERJOC02_30_07_2012  2012-07-30        BRAC 1523
## 555 VERJOC03_01_08_2017  2017-08-01         SUB 1911
## 556 VERJOC03_08_08_2019  2019-08-08         SUB 1911
## 557 VERJOC03_27_07_2016  2016-07-27         SUB 1911
## 558 VERJOC03_29_07_2015  2015-07-29         SUB 1911
## 559 VERJOC03_30_07_2012  2012-07-30         SUB 1911
## 560 VERJOC03_31_07_2014  2014-07-31         SUB 1911
```

Les mesures réalisées trop tôt ou trop tard sont pas exploitables. Mieux vaut sauter une année si la saison est passée.


![](README_files/figure-html/unnamed-chunk-17-1.png)<!-- -->


## filtrer les releves effectués trop tôt ou trop tard

```
## 
##  ALP BOMB BRAC EBOU ECOR ENHE  HUM  NAR  NIV PROD  QUE  SUB 
##   59   24    7    3   66   37    1   38   23  102   36  108
```

```
## # A tibble: 21 × 3
##    ref_site nsites ref_typoveg
##    <chr>     <int> <chr>      
##  1 VANVAL04      7 NIV        
##  2 VANVAL03      6 ENHE       
##  3 VERJOC03      6 SUB        
##  4 ECRCHA02      4 PROD       
##  5 VERDAR01      4 SUB        
##  6 VERDAR03      4 SUB        
##  7 BELRIV01      3 QUE        
##  8 BELRIV02      3 QUE        
##  9 BELRIV03      3 SUB        
## 10 ECRSUR03      3 SUB        
## # ℹ 11 more rows
```

A réfléchir comment gérer les contraintes...


## Modèle général de biomasse, SPATIAL (variations de hauteur)


```
## 
## Call:
##  randomForest(formula = hmean ~ ., data = releves_selvars_mod_spat,      ntree = 100, mtry = 16) 
##                Type of random forest: regression
##                      Number of trees: 100
## No. of variables tried at each split: 16
## 
##           Mean of squared residuals: 10.77183
##                     % Var explained: 83.67
```

![](README_files/figure-html/unnamed-chunk-19-1.png)<!-- -->

![](README_files/figure-html/prop_typevarspa-1.png)<!-- -->

![](README_files/figure-html/prop_tempvarspa-1.png)<!-- -->

83% de variance expliqué spatialement. Effet important du type de vegetation, et de temperatures notamment frost

On a pas mal de limites liées au démarrage de la saison de végétation.

## Modèle général de variation de biomasse inter-annuelle (retrait effet site)

Etendu du jeu de données temporel

![](README_files/figure-html/obsperplot-1.png)<!-- -->

![](README_files/figure-html/generalModel-1.png)<!-- -->

![](README_files/figure-html/prop_typevartemp-1.png)<!-- -->

![](README_files/figure-html/prop_tempvartemp-1.png)<!-- -->


Autour de 22% de variance temporelle expliquée. Globalement le climat du mois de juin semble le plus déterminant, ainsi que les precipitations au début de la saison. On retrouve un gros effet "année" également.




Analyse par type de facteurs






### Sélection de modèle avec toutes les variables selectionnées dans les 10 modèles pour chaque milieu






#### Par groupe de variables par milieu


![](README_files/figure-html/final -1.png)<!-- -->

On retrouve une importance des températures (mini, maxi, ...) pour la plupart des milieux.
- Tmax pour ECOR, NIV ENHE et PROD et SUB
- Tmin pour PROD, SUB, NAR, BOMB et un peu ENHE
- Le gdd pour ALP, NAR, NIV, ECOR et un peu BOMB QUE

La neige entre en jeu pour la plupart des milieux mais de façon différente
- La quantité accumulée pour BOMB SUB et NAR
- La date de fonte pour PROD
- La vitesse d'atteinte gdd300 pour ALP

Les précipitations de début de saison sont importantes pour les milieux QUE surtout. 

On a pas mal d'effets "années" car les variables "growing season" sont des variables compilées sur tout l'été (couleurs claires). Cela est moins évident pour ECOR et NIV

#### Sens des liens avec les variables explicatives

![](README_files/figure-html/sensVars-1.png)<!-- -->


### Messages conclusion:

- Différences entre milieux.



- Ce serait un plus d'avoir des fiches par stations pour mieux caractériser le type de milieu (qq espèces dominantes, classe type DELPHINE, type d'habitat phytosocio, ...)


# Analyser la typo
=======


Les prairies productives

```
##     ref_site
## 1   ECRCHA01
## 9   ECRCHA02
## 17  ECRCHA09
## 25  ECRCRO02
## 28  ECRCRO03
## 31  ECRLAN02
## 34  MERALP04
## 35  MERCHA03
## 36  MERPIS02
## 37  VANAVA01
## 45  VANAVA02
## 53  VANCHA01
## 60  VANLEC02
## 65  VANPLA01
## 73  VANPLA03
## 81  VANROS01
## 87  VANVAL01
## 91  VERJOC01
## 99  VERMOL01
## 106 VERMOL02
```

![](README_files/figure-html/vegtypePROD-1.png)<!-- -->

Les Nardaies

```
##    ref_site
## 1  ECRCHA10
## 9  ECRROU01
## 12 ECRROU04
## 15 MERCHA02
## 17 MERENF01
## 18 MERSAN01
## 20 UBABAS01
## 23 UBABAS02
## 26 VANPLA02
## 33 VANROS02
## 39 VERMOL04
```

![](README_files/figure-html/vegtypeNAR-1.png)<!-- -->

## Effets spatiaux purs: typo de végétation et nos variables

![](README_files/figure-html/unnamed-chunk-22-1.png)<!-- -->

On a surtout un axe qui ressort et est en lien avec les variables de température. De façon intéressante, le deuxième axe fait ressortir les minimales/ gel avec un large degré d'indépendance donc.


![](README_files/figure-html/unnamed-chunk-23-1.png)<!-- -->

Les types de végétation se répartissent assez bien le long de cet axe principal, avec une position similaire pour PROD, QUE, NAR, et ECOR, qui se différencient plus ou moins sur leur tolérance au gel.

Les PROD sont les plus hétérogènes sur l'axe 1.
Les PROD/NAR et QUE et ECOR se distinguent par l'axe 2 (gel/ temp mini)

Répartition des sites:
![](README_files/figure-html/unnamed-chunk-24-1.png)<!-- -->


Discimination des types de végétation:

Qu'est ce qui disciminent le plus les types de végétation?

![](README_files/figure-html/unnamed-chunk-25-1.png)<!-- -->![](README_files/figure-html/unnamed-chunk-25-2.png)<!-- -->

Encore une fois, on retrouve le fait que QUE/PROD et NAR sont peu discriminées par les variables env., surtout PROD englobe NAR et QUE.

<!-- Répartition des sites: -->
<!-- ```{r , fig=TRUE} -->
<!-- ggplot(AD$li, aes(DS1, DS2, color = dat_pca$ref_typoveg, label = rownames(dat_pca) )) + -->
<!--          geom_label_repel(aes(DS1, DS2, label = rownames(dat_pca) ), fontface = 'bold', -->
<!--                    box.padding = unit(0.35, "lines"), -->
<!--                    point.padding = unit(0.5, "lines"), -->
<!--                    segment.color = 'grey50',  -->
<!--                    max.overlaps = 30) + -->
<!--   scale_shape_manual(values=1:8) + -->
<!--   geom_point(size=2, stroke = 2) + -->
<!--   xlab(paste0("Axe1: 68% variance")) + -->
<!--   ylab(paste0("Axe2: 14% variance")) -->

<!-- ``` -->



## Effets années


![](README_files/figure-html/unnamed-chunk-26-1.png)<!-- -->

En prenant en compte les effets temporels, les axes restent du même genre mais on a moins de corrélations.


![](README_files/figure-html/unnamed-chunk-27-1.png)<!-- -->

On retrouve ici des années typiques :

  - 2013 très tardive (enneigement long)
  - 2018 très chaud au démarrage
  - 2015 dénneigement précoce 
  - 2012/2014/2016 gel tardif

<!-- - 2013: pas d'herbe en début de saison (démarrage tardif car printemps froid) -->
<!-- - 2017-2019: chaud et sec -->
<!-- - 2019: très sec au démarrage (très peu de neige) -->


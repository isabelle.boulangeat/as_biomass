releves_all = readRDS("releves_all_plus.rds")
head(releves_all)
dim(releves_all)
length(unique(releves_all$releve))
length(unique(releves_all$id_site))

# CHECK NA
head(releves_all[which(is.na(releves_all$hmean)),])
#


library(ggplot2)
ggplot(releves_all, aes(x = reorder(ref_typoveg, cumgdd_60d, na.rm=TRUE), y = cumgdd_60d)) +
  geom_boxplot() +
  labs(y="gdd", x="Type vegetation")

ggplot(releves_all, aes(x = reorder(ref_typoveg, time_after_LSD, na.rm=TRUE), y = time_after_LSD)) +
    geom_boxplot() +
    labs(y="time_after_LSD", x="Type vegetation")

ggplot(releves_all, aes(x = reorder(ref_typoveg, LSD, na.rm=TRUE), y = LSD)) +
  geom_boxplot() +
  labs(y="LSD", x="Type vegetation")


as.Date(80, origin = "2018-01-01")
as.Date(120, origin = "2018-01-01")
as.Date(160, origin = "2018-01-01")


vars = c("alti", "slope", "lf", "northing", "easting")
vars = colnames(releves_all)[25:ncol(releves_all)]


library(ade4)
dat = na.omit(releves_all[,vars])
pca = dudi.pca(dat, scan=FALSE, nf = 4)
inertia.dudi(pca, row=FALSE, col =TRUE)
par(mfrow=c(1,2))
s.corcircle(pca$co)
s.corcircle(pca$co, xax=3, yax=4)
## ax1 44% gddday, gdd, temp
## ax2 12% time after lsd, gddspeed
## ax3 8% frost severe, rainfall
## ax4 6% ...
# other variables not represented : slope, north/east,

### ==== MODELE EXPLORATION

dat = na.omit(releves_all[,c(vars, "hmean", "ref_typoveg")])
# dat$lf = as.factor(dat$lf)
dat$ref_typoveg = as.factor(dat$ref_typoveg)
dim(dat)
library(randomForest)
mod = randomForest(hmean ~ . , data = dat, ntree = 100, mtry=4)
plot(mod$rsq, type = "l", xlab = "nombre d'arbres", ylab = "erreur OOB")
mod = randomForest(hmean ~ . , data = dat, ntree = 40, mtry=10)
mod
varImpPlot(mod)


### remove typo effect
dat = na.omit(releves_all[,c(vars, "hmean", "ref_typoveg")]) %>% 




library(plotmo)
plotmo(mod, ylim = NA)

#### avec ref typoveg
mod = randomForest(hmean ~ gddspeed.gdd300 + radiations.gdd300 + cumgdd60d + rainfall.gdd300 + rainfall.gdd900 + frost_severe.gdd300 + slope + northing + easting + lf + ref_typoveg, data = dat, ntree = 100, mtry=4)
# plot(mod$rsq, type = "l", xlab = "nombre d'arbres", ylab = "erreur OOB")
mod



pkgs_CRAN <- c("lme4","MCMCglmm","blme",
               "pbkrtest","coda","aods3","bbmle","ggplot2",
               "reshape2","plyr","numDeriv","Hmisc",
               "plotMCMC","gridExtra","R2admb",
               "broom.mixed","dotwhisker")
install.packages(pkgs_CRAN)


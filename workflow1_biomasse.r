####  -> cf readme
sapply(list.files("R_fct"), function(x)source(paste0("R_fct/",x)))
#==============================
## dataframe ##
#===============================
sites = read.csv("_data_raw/sentinelle_biomasse_sites.csv")
data_h = read.csv("_data_prod/data_biomass.csv")


dat_h_veg = unique(merge(data_h, sites[,c("id_site", "ref_typoveg")], by.x = "ref_site", by.y ="id_site"))

length(unique(dat_h_veg$releve))
dim(dat_h_veg)

summary(dat_h_veg$hmean)
dat_h_veg[is.na(dat_h_veg$hmean),]

#==============================

library(dplyr)
# library(reshape)
library(tidyr)
library(ggplot2)

#==============================
## variation par milieu des mesures biomasse ##
#===============================

dat_long <- dat_h_veg %>%
 dplyr::select(hmean, hmad, ref_typoveg) %>%
 drop_na() %>%
 group_by(ref_typoveg) %>%
 summarize(hauteur=mean(hmean), var_inter=mad(hmean), var_intra = mean(hmad)) %>%
 gather(stat, value, hauteur:var_intra)

head(dat_long)

# 
# ggplot(dat_long, aes(fill=stat, x=ref_typoveg, y=value)) +
#   geom_bar(position = "dodge", stat = "identity") +
#   facet_wrap(~stat, ncol = 1, scales = "free")
# 
# ## boxplot par milieu ##
# 
# ggplot(dat_h_veg, aes(x = reorder(ref_typoveg, hmean, na.rm=TRUE), y = hmean)) +
#   geom_boxplot() +
#   labs(y="Hauteur moyenne (est. biomasse)", x="Type vegetation")
# 
# 
# ## H series par milieu ##
# 
# pl <- ggplot(dat_h_veg, aes(x = date_releve, y = hmean)) +
#   geom_line(aes(color = ref_site), show.legend = FALSE) +
#   facet_wrap(~ref_typoveg)
# 
# pl + theme(legend.position = "none")
# 
# 
#==============================
## effet obs  / precision mesure   ##
#===============================

calage = read.csv("../PNE_calage_reformat.csv", sep= ";", dec = ",")
head(calage)
str(calage)
calage$releve = paste0(calage$site, calage$Releveur)

cal_mean = unique(calage %>% group_by(releve) %>% summarize(site = site, hmean = mean(hauteur, na.rm=TRUE)))
# 
# ggplot(cal_mean, aes(x = reorder(site, hmean), y = hmean)) +
#   geom_boxplot() +
#   geom_point()
# 
obs_error = cal_mean %>% group_by(site) %>% summarize(hmad = mad(hmean), hmad_st = mad(hmean)/mean(hmean), hm = mean(hmean))
# 
# ggplot(obs_error, aes(x = reorder(site, hm), y = hmad_st)) +
#   geom_bar(stat = "identity")

#==============================
## heterogeneité                ##
#===============================
# 
# ### sd vs mad
# ggplot(dat_h_veg, aes(x = hmad, y = hsd)) +
#   geom_point()
# 
# ### dans le temps
# ggplot(dat_h_veg, aes(x = date_releve, y = hmad)) +
#   geom_line(aes(color = ref_site), show.legend = FALSE) +
#   facet_wrap(~ref_typoveg)
# 
# ## boxplot par milieu ##
# 
# ggplot(dat_h_veg, aes(x = reorder(ref_typoveg, hmad, na.rm=TRUE), y = hmad)) +
#   geom_boxplot() +
#   labs(y="Variation intra de hauteur", x="Type vegetation")
# 
# 
# ggplot(dat_h_veg, aes(x = reorder(ref_typoveg, hmad/hmean, na.rm=TRUE), y = hmad/hmean)) +
#   geom_boxplot() +
#   labs(y="Variation intra de hauteur (stand. moy.)", x="Type vegetation")
